﻿using Unity.Netcode;
using UnityEngine;

public class StateMsg : INetworkSerializable
{
    public Vector2 Position;
    public Vector2 Velocity;
    public int TickNumber;

    public StateMsg() { }

    public StateMsg(Vector2 position, Vector2 velocity, int tickNumber)
    {
        Position = position;
        Velocity = velocity;
        TickNumber = tickNumber;
    }

    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref Position);
        serializer.SerializeValue(ref Velocity);
        serializer.SerializeValue(ref TickNumber);
    }
}
