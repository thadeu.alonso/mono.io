using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class LobbyUI : NetworkBehaviour
{
    public Camera lobbyCamera;

    NetworkManager networkManager;

    void Start()
    {
        networkManager = NetworkManager.Singleton;
    }

    public void OnHostClicked()
    {
        networkManager.StartHost();
        DisableCanvas();
    }

    public void OnClientClicked()
    {
        networkManager.StartClient();
        DisableCanvas();
    }

    public void OnServerClicked()
    {
        networkManager.StartServer();
        DisableCanvas();
    }

    private void DisableCanvas()
    {
        lobbyCamera.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
