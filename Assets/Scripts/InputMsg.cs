﻿using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class InputMsg : INetworkSerializable
{
    public Vector2[] Inputs;
    public int StartTick;

    public InputMsg() { }

    public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
    {
        serializer.SerializeValue(ref Inputs);
        serializer.SerializeValue(ref StartTick);
    }
}