using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class NetworkAim : NetworkBehaviour
{
    public Transform gunPivot;
    public Camera playerCamera;

    public Vector3 MouseDirection { get; private set; }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        enabled = IsOwner;
    }

    private void Update()
    {
        Vector3 mousePosition = playerCamera.ScreenToWorldPoint(Input.mousePosition);
        MouseDirection = (mousePosition - transform.position).normalized;
        float angle = Mathf.Atan2(MouseDirection.y, MouseDirection.x) * Mathf.Rad2Deg;
        gunPivot.eulerAngles = new Vector3(0f, 0f, angle);
    }
}