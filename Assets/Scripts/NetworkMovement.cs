using System;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class NetworkMovement : NetworkBehaviour
{
    private const int BUFFER_SIZE = 1024;

    public float moveSpeed;

    Rigidbody2D rigidbody2D;
    Camera playerCamera;

    float timer;
    int tickNumber;
    int lastReceivedStateTick;
    Vector2 currentInput;
    Vector2 inputLastTick;
    Queue<InputMsg> inputQueue = new Queue<InputMsg>();
    Queue<StateMsg> statesQueue = new Queue<StateMsg>();
    StateMsg[] clientStateBuffer;
    Vector2[] inputBuffer;

    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        timer = 0f;
        tickNumber = 0;
        clientStateBuffer = new StateMsg[BUFFER_SIZE];
        inputBuffer = new Vector2[BUFFER_SIZE];

        for (int i = 0; i < BUFFER_SIZE; i++)
        {
            clientStateBuffer[i] = new StateMsg();
            inputBuffer[i] = Vector2.zero;
        }
    }

    private void Update()
    {
        if(IsClient)
        { 
            timer += Time.deltaTime;

            while (timer >= Time.fixedDeltaTime)
            {
                timer -= Time.fixedDeltaTime;

                float horInput = Input.GetAxisRaw("Horizontal");
                float verInput = Input.GetAxisRaw("Vertical");
                currentInput = new Vector2(horInput, verInput);

                if(currentInput != inputLastTick)
                {
                    InputMsg inputMsg = new InputMsg
                    {
                        StartTick = lastReceivedStateTick
                    };
                    
                    List<Vector2> inputs = new List<Vector2>();

                    for (int tick = lastReceivedStateTick; tick <= tickNumber; ++tick)
                    {
                        inputs.Add(inputBuffer[tick % BUFFER_SIZE]);
                    }

                    inputMsg.Inputs = inputs.ToArray();

                    SendInputServerRpc(inputMsg);
                    MovePlayer(currentInput);
                }

                int bufferSlot = tickNumber % BUFFER_SIZE;
                inputBuffer[bufferSlot] = currentInput;
                clientStateBuffer[bufferSlot].Position = rigidbody2D.position;
                clientStateBuffer[bufferSlot].Velocity = rigidbody2D.velocity;

                inputLastTick = currentInput;
                Physics.Simulate(Time.fixedDeltaTime);
                ++tickNumber;
            }

            while (HasAvailableStateMessages())
            {
                StateMsg stateMsg = statesQueue.Dequeue();
                lastReceivedStateTick = stateMsg.TickNumber;

                int bufferSlot = stateMsg.TickNumber % BUFFER_SIZE;
                Vector2 positionError = stateMsg.Position - clientStateBuffer[bufferSlot].Position;

                if (positionError.sqrMagnitude > 0.0000001f)
                {
                    rigidbody2D.position = stateMsg.Position;
                    rigidbody2D.velocity = stateMsg.Velocity;

                    int rewindTickNumber = stateMsg.TickNumber;

                    while (rewindTickNumber < tickNumber)
                    {
                        bufferSlot = rewindTickNumber % BUFFER_SIZE;
                        inputBuffer[bufferSlot] = currentInput;
                        clientStateBuffer[bufferSlot].Position = rigidbody2D.position;
                        clientStateBuffer[bufferSlot].Velocity = rigidbody2D.velocity;

                        MovePlayer(currentInput);

                        Physics.Simulate(Time.fixedDeltaTime);
                        ++rewindTickNumber;
                    }
                }
            }
        }

        if(IsServer)
        {
            while (HasAvailableInputMessages())
            {
                InputMsg inputMsg = inputQueue.Dequeue();

                int maxTick = inputMsg.StartTick + inputMsg.Inputs.Length - 1;

                if(maxTick >= tickNumber)
                {
                    int startTick = tickNumber > inputMsg.StartTick ? (tickNumber - inputMsg.StartTick) : 0;

                    for (int i = startTick; i < inputMsg.Inputs.Length; ++i)
                    {
                        MovePlayer(inputMsg.Inputs[i]);
                        Physics.Simulate(Time.fixedDeltaTime);
                    }

                    tickNumber = maxTick + 1;
                }

                //StateMsg stateMsg = new StateMsg(rigidbody2D.position, rigidbody2D.velocity, inputMsg.TickNumber + 1);
                //SendPlayerStateClientRpc(stateMsg);
            }
        }
    }


    private void MovePlayer(Vector2 input)
    {
        rigidbody2D.velocity = input * moveSpeed;
    }
    private bool HasAvailableStateMessages()
    {
        return statesQueue.Count > 0;
    }

    private bool HasAvailableInputMessages()
    {
        return inputQueue.Count > 0;
    }

    [ClientRpc]
    private void SendPlayerStateClientRpc(StateMsg stateMsg)
    {
        statesQueue.Enqueue(stateMsg);
    }

    [ServerRpc]
    private void SendInputServerRpc(InputMsg inputMsg)
    {
        inputQueue.Enqueue(inputMsg);
    }

    public override void OnNetworkSpawn()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        playerCamera = GetComponentInChildren<Camera>();

        if (!IsLocalPlayer)
            playerCamera.gameObject.SetActive(false);
    }
}