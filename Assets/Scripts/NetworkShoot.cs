using Unity.Netcode;
using UnityEngine;

public class NetworkShoot : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public Transform firePoint;
    public NetworkAim aim;

    [Header("Shoot settings")]
    [SerializeField] float shootForce;
    [SerializeField] float shootRate;

    NetworkVariable<float> shootRateTimer = new NetworkVariable<float>(0f);

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && CanShoot())
            RequestBulletShotServerRpc(firePoint.position, aim.MouseDirection);
    }

    private void FixedUpdate()
    {
        if(IsServer)
        {
            if (shootRateTimer.Value > 0f)
                shootRateTimer.Value -= Time.fixedDeltaTime;
            else
                shootRateTimer.Value = 0f;
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void RequestBulletShotServerRpc(Vector2 spawnPosition, Vector2 direction)
    {
        var bullet = Instantiate(bulletPrefab, spawnPosition, Quaternion.identity);
        var bulletRgbd = bullet.GetComponent<Rigidbody2D>();
        var bulletNetObj = bullet.GetComponent<NetworkObject>();

        bulletRgbd.bodyType = RigidbodyType2D.Dynamic;
        bulletRgbd.velocity = Vector2.zero;
        bulletRgbd.AddRelativeForce(direction * shootForce, ForceMode2D.Impulse);

        bulletNetObj.Spawn();
        shootRateTimer.Value = shootRate;
    }

    private bool CanShoot()
    {
        return shootRateTimer.Value <= 0f;
    }
}